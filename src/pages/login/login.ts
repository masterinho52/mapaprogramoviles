import { HomePage } from "./../home/home";
import { UsuarioProvider } from "./../../providers/usuario/usuario";
import { Component } from "@angular/core";
import {
  IonicPage,
  NavController,
  NavParams,
  LoadingController
} from "ionic-angular";
import { ViewChild } from "@angular/core";
import { Slides } from "ionic-angular";

import { AlertController } from "ionic-angular";
import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: "page-login",
  templateUrl: "login.html"
})
export class LoginPage {
  user: any;
  @ViewChild(Slides)
  slides: Slides;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private alertCtrl: AlertController,
    public loadingCtrl: LoadingController,
    public _usuarioprovider: UsuarioProvider
  ) {
    this.mostrarInput();
  }

  ionViewDidLoad() {
    console.log("ionViewDidLoad LoginPage");
    this.slides.pager = true;
    this.slides.paginationType = "progress";
    this.slides.lockSwipes(true);
    this.slides.freeMode = false;
  }

  mostrarInput() {
    let alert = this.alertCtrl.create({
      title: "Login",
      inputs: [
        {
          name: "username",
          placeholder: "Username"
        },
        {
          name: "password",
          placeholder: "Password",
          type: "password"
        }
      ],
      buttons: [
        {
          text: "Cancel",
          role: "cancel",
          handler: data => {
            console.log("Cancel clicked");
          }
        },
        {
          text: "Login",
          handler: data => {
            this.user = JSON.stringify(data);
            console.log(" accept login " + data); //to see the object
            if (data.username && data.password) {
              this.validarusuario(data);
            } else {
              // invalid login
              return false;
            }
          }
        }
      ]
    });
    alert.present();
  }

  validarusuario(data: any) {
    let loading = this.loadingCtrl.create({
      content: "Validando usuario"
    });
    loading.present();
    console.log("dentro de validar usuario " + data);
    this._usuarioprovider.verificarusuario(data.username).then(existe => {
      loading.dismiss();
      console.log("existe " + existe);
      if (existe) {
        this.slides.lockSwipes(false);
        this.slides.freeMode = true;
        this.slides.slideNext();
        this.slides.lockSwipes(true);
        this.slides.freeMode = false;
      } else {
        this.alertCtrl
          .create({
            title: "Usuario Incorrecto",
            subTitle: "Intente nuevamente",
            buttons: [
              {
                text: "Aceptar",
                handler: () => {
                  //this.mostrarInput();
                }
              }
            ]
          })
          .present();
      }
    });

    setTimeout(() => {
      loading.dismiss();
    }, 3000);

    if (data.username == "jramirez" && data.password == "123") {
      console.log("validacion correcta");
    } else {
      console.log("datos incorrectos");
    }
  }

  ingresar() {
    this.navCtrl.setRoot(HomePage);
  }
}
