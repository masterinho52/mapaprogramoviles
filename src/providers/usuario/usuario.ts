import { Platform } from "ionic-angular";
import { Injectable } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Storage } from "@ionic/storage";
/*
  Generated class for the UsuarioProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UsuarioProvider {
  clave: string;
  datauser: any = {};

  constructor(
    private angularfiredb: AngularFirestore,
    private plataforma: Platform,
    private storage: Storage
  ) {
    console.log("Hello UsuarioProvider Provider");
  }

  verificarusuario(clave: string) {
    clave = clave.toLowerCase();
    console.log("verificar usuario, clave =" + clave);

    return new Promise((resolve, reject) => {
      this.angularfiredb
        .doc(`/usuarios/${clave}`)
        .valueChanges()
        .subscribe(data => {
          if (data) {
            console.log(data);
            this.clave = clave;
            this.datauser = data;
            this.escribirDataLocal();
            resolve(true);
          } else resolve(false);
        });
    });
  }

  escribirDataLocal() {
    if (this.plataforma.is("cordova")) {
      //dispositivo
      this.storage.set("clave", this.clave);
    } else {
      //navegador
      localStorage.setItem("clave", this.clave);
    }
  }

  leerDataLocal() {
    return new Promise((resolve, reject) => {
      if (this.plataforma.is("cordova")) {
        //dispositivo
        this.storage.get("clave").then(val => {
          if (val) {
            this.clave = val;
            resolve(true);
          } else {
            resolve(false);
          }
        });
      } else {
        //navegador
        if (localStorage.getItem("clave")) {
          this.clave = localStorage.getItem("clave");
          resolve(true);
        } else {
          resolve(false);
        }
      }
    });
  }
}
