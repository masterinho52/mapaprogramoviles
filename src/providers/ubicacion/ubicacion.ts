import { UsuarioProvider } from "./../usuario/usuario";
import { Injectable } from "@angular/core";
import { Geolocation } from "@ionic-native/geolocation";
import {
  AngularFirestore,
  AngularFirestoreDocument
} from "@angular/fire/firestore";
import { clearOverrides } from "@angular/core/src/view";

/*
  Generated class for the UbicacionProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class UbicacionProvider {
  carro: AngularFirestoreDocument<any>;

  constructor(
    private geolocation: Geolocation,
    public _usuarioProvider: UsuarioProvider,
    private AngularFSDB: AngularFirestore
  ) {
    console.log("Hello UbicacionProvider Provider");
    this.carro = AngularFSDB.doc("/usuarios/${_usuarioProvider.clave}");
  }

  iniciarGeolocalizacion() {
    this.geolocation
      .getCurrentPosition()
      .then(resp => {
        //resp.coords.latitude
        //resp.coords.longitude
        console.log(resp.coords);
        this.carro.update({
          lat: resp.coords.latitude,
          lng: resp.coords.longitude,
          clave: this._usuarioProvider.clave
        });
      })
      .catch(error => {
        console.log("Error getting location ", error);
      });
    let watch = this.geolocation.watchPosition();
    watch.subscribe(data => {
      // data can be a set of coordinates, or an error (if an error occurred).
      // data.coords.latitude
      // data.coords.longitude
      this.carro.update({
        lat: data.coords.latitude,
        lng: data.coords.longitude,
        clave: this._usuarioProvider.clave
      });
      console.log(data.coords);
    });
  }
}
